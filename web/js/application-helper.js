var app_ = {
    pushNotification: null,
    inAppBrowserRef: null,
    get_device_type: function () {
        console.log(navigator.userAgent);
        return navigator.userAgent;
    },
    get_map_location: function (address, lat, log) {
        var output = 'Address : ' + address + ' Lat:' + lat + 'Long:' + log;
        console.log(output);
    },
    find_out_more: function (target) {
        console.log(target);
    },
    get_uuid: function () {
        return 'XRTF5854rtyh487SS#$%WW';
    },
    get_push_token: function () {
        return app_.storage_get('pn');
    },

    loadStartCallBack: function () {

        uiBlocker();
    },

    loadStopCallBack: function () {

        if (app_.inAppBrowserRef != undefined) {
            app_.inAppBrowserRef.insertCSS({code: "body{font-size: 25px;"});
            app_.inAppBrowserRef.show();
        }

    },

    loadErrorCallBack: function (params) {

        //$('#status-message').text("");
        //uiUnBlocker();

        var scriptErrorMesssage =
                "alert('Sorry we cannot open that page. Message from the server is : "
                + params.message + "');"

        app_.inAppBrowserRef.executeScript({code: scriptErrorMesssage}, app_.executeScriptCallBack);

        app_.inAppBrowserRef.close();

        app_.inAppBrowserRef = undefined;

    },

    executeScriptCallBack: function (params) {

        if (params[0] == null) {

            $('#status-message').text(
                    "Sorry we couldn't open that page. Message from the server is : '"
                    + params.message + "'");
        }

    },

    http_get: function (url, parameters, onsuccess, onerror) {
        $.ajaxSetup({
            beforeSend: function (xhr) {
                xhr.setRequestHeader('authToken', app_.storage_get('authToken'));
            }
        });
        $.ajax({url: url,
            method: "GET", data: parameters, success: function (data) {
                onsuccess(data);
            }});
    },
    http_post: function (url, parameters, onsuccess, onerror) {
        $.ajax({url: url, method: "POST", data: parameters, success: function (data) {
                onsuccess(data);
            }});
    },
    storage_set: function (key, val) {
        window.localStorage.setItem(key, val);
    },
    storage_get: function (key) {
        return window.localStorage.getItem(key);
    },
    check_connection: function () {
        var networkState = navigator.connection.type;
        var states = {};
        states[Connection.UNKNOWN] = 'Unknown connection';
        states[Connection.ETHERNET] = 'Ethernet connection';
        states[Connection.WIFI] = 'WiFi connection';
        states[Connection.CELL_2G] = 'Cell 2G connection';
        states[Connection.CELL_3G] = 'Cell 3G connection';
        states[Connection.CELL_4G] = 'Cell 4G connection';
        states[Connection.CELL] = 'Cell generic connection';
        states[Connection.NONE] = 'No network connection';
        return 'Connection type: ' + states[networkState];
    },
    onfocus_: function () {

    },
    onload_: function () {
        //autoReloadActivit();

        return true;
        if ((app_.storage_get('session_token') !== null) || (app_.storage_get('user_id') !== null)) {
            //$scope.reloadActivities('stream',false);
            alert();
            //window.location = '#/activity';
        }

    },
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

};
function tokenHandler(result) {
    // Your iOS push server needs to know the token before it can push to this device
    // here is where you might want to send it the token for later use.
    alert('device token = ' + result);
}

function successHandler(result) {
    alert('result = ' + result);
}

function errorHandler(error) {
    alert('error = ' + error);
}

function onNotificationAPN(event) {
    if (event.alert)
    {
        navigator.notification.alert(event.alert);
    }

    if (event.sound)
    {
        var snd = new Media(event.sound);
        snd.play();
    }

    if (event.badge)
    {
        pushNotification.setApplicationIconBadgeNumber(successHandler, errorHandler, event.badge);
    }
}

function onNotification(e) {
    $("#app-status-ul").append('<li>EVENT -> RECEIVED:' + e.event + '</li>');
    switch (e.event)
    {
        case 'registered':
            if (e.regid.length > 0)
            {
                $("#app-status-ul").append('<li>REGISTERED -> REGID:' + e.regid + "</li>");
                // Your GCM push server needs to know the regID before it can push to this device
                // here is where you might want to send it the regID for later use.
                console.log("regID = " + e.regid);
            }
            break;
        case 'message':
            // if this flag is set, this notification happened while we were in the foreground.
            // you might want to play a sound to get the user's attention, throw up a dialog, etc.
            if (e.foreground)
            {
                $("#app-status-ul").append('<li>--INLINE NOTIFICATION--' + '</li>');
                // on Android soundname is outside the payload.
                // On Amazon FireOS all custom attributes are contained within payload
                var soundfile = e.soundname || e.payload.sound;
                // if the notification contains a soundname, play it.
                var my_media = new Media("/android_asset/www/" + soundfile);
                my_media.play();
            } else
            {  // otherwise we were launched because the user touched a notification in the notification tray.
                if (e.coldstart)
                {
                    $("#app-status-ul").append('<li>--COLDSTART NOTIFICATION--' + '</li>');
                } else
                {
                    $("#app-status-ul").append('<li>--BACKGROUND NOTIFICATION--' + '</li>');
                }
            }

            $("#app-status-ul").append('<li>MESSAGE -> MSG: ' + e.payload.message + '</li>');
            //Only works for GCM
            $("#app-status-ul").append('<li>MESSAGE -> MSGCNT: ' + e.payload.msgcnt + '</li>');
            //Only works on Amazon Fire OS
            $status.append('<li>MESSAGE -> TIME: ' + e.payload.timeStamp + '</li>');
            break;
        case 'error':
            $("#app-status-ul").append('<li>ERROR -> MSG:' + e.msg + '</li>');
            break;
        default:
            $("#app-status-ul").append('<li>EVENT -> Unknown, an event was received and we do not know what it is</li>');
            break;
    }
}

