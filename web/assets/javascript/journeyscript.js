var blured = false;
var blured2 = false;
var scrollblur = false;
var isUploadNew = false;
function resetFormSaveBinding() {

    $(document).off("click", ".save-j-update");
    $(".save-j-update").unbind("click");
    $(document).on("click", ".save-j-update", function() {
        //debugger;
        if ($(this).hasClass("story-edit") == true) { // we are editing the strory 
            if ($(this).hasClass('update-story') == true) {
                return false;
            }
            $(this).addClass("update-story");

            if ($.trim($("#incogo_member_journey_update_journey_title").val()) === "") {
                genericDialogue("Incorrect Title", "This journey title appears to be blank. Please enter journey title to update your story.");
                $("#incogo_member_journey_update_journey_title").val('');
                $("#incogo_member_journey_update_journey_title").focus();
                $("#incogo_member_journey_update_journey_title").addClass('error');
                $(this).removeClass('update-story');
                return false;
            }

            if ($("#incogo_member_journey_update_journey_description").val().length > 220 && $("#incogo_member_journey_update_journey_title").val().length > 23) {
                genericDialogue("Exceeded Character Limit", "You have exceeded the character limit. Journey titles are to be kept to 23 characters and journey stories are to be no more than 220 characters.");
                $(this).removeClass('update-story');
                return false;
            } else if ($("#incogo_member_journey_update_journey_title").val().length > 23) {
                genericDialogue("Incorrect Title", "This journey title should not be more than 23 characters.");
                $(this).removeClass('update-story');
                return false;
            } else if ($("#incogo_member_journey_update_journey_description").val().length > 220) {
                genericDialogue("Incorrect Description", "This journey description should not be more than 220 characters.");
                $(this).removeClass('update-story');
                return false;
            }
            if ($(this).hasClass('is-story-update')) {
                if ($.trim($("#incogo_member_journey_update_journey_description").val()) === "") {
                    $("#incogo_member_journey_update_journey_description").val('');
                }
                $("#frm_journey_update").submit();
                return;
            } else if (isFileUpload === 'no' && !isMobile.AndroidApp()) {
                genericDialogue('Error', 'Please choose a file first.');
                $(this).removeClass('update-story');
            }

            //mobile app
            if (isMobile.AndroidApp() && !$("#androidImage").length) {
                genericDialogue('Error', 'Please choose a file first.');
                $(this).removeClass('update-story');
            } else if (isMobile.AndroidApp() && $("#androidImage").length) {
                $("#frm_journey_update").submit();
                $(this).removeClass('update-story');
                return false;
            }

        }
    });

}


$(function() {
   // resetFormSaveBinding();
    $(document).on("click", "[data-menuid]", function(e) {
        e.stopPropagation();
        if ($("#common-drop-down").html() != '') {
            $("#common-drop-down").slideUp('fast', function(){
                $("#common-drop-down").html('');
                $('.menu-background').eq(0).remove();
                if($("#post-comment-iphone").is(':visible')){
                    $("#post-comment-iphone").css({"z-index":""});
                }
            });
        }
        var openMenu = $(this).attr("data-menuid");
        if (openMenu == "#choose-photo") {
            $("html, body").scrollTop(0);
        }
        if (openMenu == "#categories-menu") {
            $(".start-content-wrap").css({"z-index":"10"});
        }
        if ($("[data-menudropdown]").is(":visible")) {
            $("[data-menuid]").removeClass("active");
            $("[data-menudropdown]").slideUp("fast");
            $('.menu-background').remove();
        }
        if ($(openMenu).is(":visible") == false) {
            $(this).addClass("active");
            $(openMenu).slideDown("fast");
            $("body").append('<div class="menu-background"></div>');
            if(openMenu == "#private-network-menu"){
                if($(e.target).attr("id") == "pn-network-id" || $(e.target).parent().attr("id") == "pn-network-id"){
                    if(isMenuOpen== true){
                        menuToggle(e);
                    }
                    $("#pn-network-id").addClass("active").parent().siblings().find("a").removeClass("active");
                    $(".floating-footer").css({"z-index":"999999"});
                }
            }
            //disable_scroll();
        } else {
            if(oldAnchor >= 0){
                $(".floating-footer li").eq(oldAnchor).children("a").addClass("active").parent().siblings().find("a").removeClass("active");
            }else {
                $(".floating-footer li").find("a").removeClass("active");
            }
            $(".floating-footer").css({"z-index":""});
            $(".start-content-wrap").css({"z-index":""});
            $(this).removeClass("active");
            $(openMenu).slideUp("fast");
            $('.menu-background').remove();
            //enable_scroll();
        }
    });
    $(document).on("click", function(e) {       
        if ($("[data-menudropdown]").is(":visible")) {
            $("[data-menuid]").removeClass("active");
            $("[data-menudropdown]").slideUp("fast");
            $('.menu-background').remove();
            if($(".start-content-wrap").is(":visible")){
                $(".start-content-wrap").css({"z-index":""});
            }
            if(oldAnchor >= 0){
                $(".floating-footer li").eq(oldAnchor).children("a").addClass("active").parent().siblings().find("a").removeClass("active");
            }else {
                $(".floating-footer li").find("a").removeClass("active");
            }
            //enable_scroll();
        }
    });
    
    $(document).on('click', '.cancel-opt-btn', function(){
        if ($("[data-menudropdown]").is(":visible")) {
            $("[data-menuid]").removeClass("active");
            $("[data-menudropdown]").slideUp("fast");
            $('.menu-background').remove();
        }
    });
    
    $(document).on("click",'a[href^="mailto:"]', function(){
            if ($("[data-menudropdown]").is(":visible")) {
                $("[data-menuid]").removeClass("active");
                $("[data-menudropdown]").slideUp("fast");
                $('.menu-background').remove();
            }
    });
    $(document).on("click touchend", ".journey-msg", function(e) {
        e.stopPropagation();
        if($(this).hasClass("promtspopups")) {
            return false;
        }
        if ($(".journey-msg").is(":visible") === true) {
            if ($("#linkarea").is(":visible")) {
                $("#linkarea").blur();
                $(".journey-msg").fadeOut(function() {
                    $("html, body").scrollTop(0);
                });
            } else {
                $(".journey-msg").fadeOut();
            }
        }
    });
    $("#linkarea").on("blur", function() {
        $("html, body").scrollTop(0);
    });
    $(document).on("click touchend", ".journey-msg-box, .promts-box", function(e) {
        e.stopPropagation();
    });
    blured = false;
    blured2 = false;
    scrollblur = false;
    $(document).on("click", "#new-tabination-menu li a", function(e) {
        if ($(this).hasClass('ignore')) {
            return false;
        }
        var whichTab = $(this).attr("data-rel");
        $("#incogo-handler").css({"height": $("#acincogo-journey").height() + "px"});
        if ($(this).parent().hasClass("active")) {
            return false;
        } else {
            if ($(this).parent().hasClass("stop-tab-switch")) {
                var currentTab = $("#new-tabination-menu li.active a").attr("data-rel");
                if (currentTab == '0') {
                    tabSwitchPopUP('Save changes', 'Save changes to your image, or cancel to revert to your previous image.');
                } else if (currentTab == '1') {
                    tabSwitchPopUP('Save changes', 'Save changes to your story, or cancel to revert to your previous story.');
                } else if (currentTab == '2') {
                    tabSwitchPopUP('Save changes', 'Save changes to your supporters, or cancel to revert to your previous supporters.');
                }
                return false;
            }
            $(this).parent().siblings().removeClass("active");
            $(this).parent().addClass("active");
            if (whichTab == '0') {
                whichTabClass(1, 0);
            } else if (whichTab == '1') {
                whichTabClass(2, 0);
                if (blured == false) {
                    if (isUploadNew) {
                        $(".blur-me-2").load(function() {
                            blurTabImage(".blur-me-2");
                            blured = true;
                            isUploadNew = false;
                            blured2 = false;
                        });
                    } else {
                        blurTabImage(".blur-me-2");
                        blured = true;
                    }
                }
            } else if (whichTab == '2') {
                whichTabClass(3, 1);

                if (blured2 == false) {
                    blurTabImage(".blur-me-3");
                    blured2 = true;
                }
            }

        }
    });
//    if (!isMobile.any()) {
        journey_scroller();
        if ($(document).scrollTop() < 245) {
            $("#incogo-handler").css({"height": $("#acincogo-journey").height() + "px"});
        }
        $(window).on("resize", function() {
            if ($(document).scrollTop() < 245) {
                $("#incogo-handler").css({"height": $("#acincogo-journey").height() + "px"});
            }
        });
        $(document).on("scroll", journey_scroller);
//    }

//    $(document).on("click", ".modal_close, .acpromts-close", function() {
//        if ($("#linkarea").is(":visible")) {
//            $("#linkarea").blur();
//            $(".journey-msg").fadeOut(function() {
//                $("html, body").scrollTop(0);
//            });
//        } else {
//            $(".journey-msg").fadeOut();
//        }
//    });

    $(document).on('click', "#invite-supporter-menu a", function(e) {
        e.preventDefault();
        if ($("#supporter-search").attr('data-personal') != undefined && $("#supporter-search").attr('data-extended') != undefined) {
            var getTab = $(this).attr("data-go-to");
            if (getTab == "tab-2") {
                if ($("#tab-2 li").length > 0) {
                    $("#supporter-search").fadeIn();
                    $(".incogo-me-journeys-2").removeClass('incogo-me-no-search');
                    $(".incogo-me-journeys-3").removeClass('incogo-me-no-search-2');
                    $(".blank-journey-message").fadeOut();
                } else {
                    $(".incogo-me-journeys-2").addClass('incogo-me-no-search');
                    $(".incogo-me-journeys-3").addClass('incogo-me-no-search-2');
                    $("#supporter-search").fadeOut();
                    $(".blank-journey-message").fadeIn();
                }
            }
            if (getTab == "tab-1") {
                if ($("#tab-1 li").length > 0) {
                    $("#supporter-search").fadeIn();
                    $(".incogo-me-journeys-2").removeClass('incogo-me-no-search');
                    $(".incogo-me-journeys-3").removeClass('incogo-me-no-search-2');
                    $(".blank-journey-message").fadeOut();
                } else {
                    $("#supporter-search").fadeOut();
                    $(".incogo-me-journeys-2").addClass('incogo-me-no-search');
                    $(".incogo-me-journeys-3").addClass('incogo-me-no-search-2');
                    $(".blank-journey-message").fadeIn();
                }
            }
        }
        if ($(e.target).hasClass("learn-icon")) {
            if ($(e.target).attr("data-rel") == "1") {
                $("#message-1").fadeIn();
            } else if ($(e.target).attr("data-rel") == "2") {
                $("#message-2").fadeIn();
            }
            return false;
        }
        if ($(this).hasClass('active') === false) {
            var goTotab = $(this).attr("data-go-to");
            $(this).parent().siblings('li').removeClass('active');
            $(this).parent().addClass('active');
            $("body").find('#' + goTotab).siblings('.select-journeys-tabs').fadeOut(function() {
                $("body").find('#' + goTotab).fadeIn();
                if (goTotab == 'tab-1') {
                    triggerGa('Personal network', 'click', 'Personal tab clicked');
                } else if (goTotab == 'tab-2') {
                    triggerGa('Personal network', 'click', 'Extended tab clicked');
                }
            });
            return false;
        }
    });

    $(document).on("keyup paste change input focus", "#incogo_member_journey_update_journey_title", function(e) {

        $(".journey-txt-counter span").text('23');
        var length = $(this).val().length;
        var totalChar = 23;
        var finalCount = totalChar - length;
        $(".journey-txt-counter span").text(finalCount);
        if (length > 23) {
            finalCount = 0;
            $(".journey-txt-counter span").text(finalCount);
            $("p.journey-txt-counter").addClass('red');
            $("#incogo_add_journey_title").val($("#incogo_add_journey_title").val().slice(0, 23));
            e.preventDefault();
            return false;
        }
        $(this).removeClass('error');
        $(".journey-txt-counter span").text(finalCount);
        if (finalCount === 0) {
            $("p.journey-txt-counter").addClass('red');
        } else {
            $("p.journey-txt-counter").removeClass('red');
        }

    });
//    $(document).on("focus", "#incogo_member_journey_update_journey_title", function(e) {
//        var length = $(this).val().length;
//        var totalChar = 23;
//        var finalCount = totalChar - length;        
//        $(".journey-txt-counter span").text(finalCount);
//    });
//    $(document).on("focus", "#incogo_member_journey_update_journey_description", function(e) {
//        var length = $(this).val().length;
//        var totalChar = 220;
//        var finalCount = totalChar - length;        
//        $(".journey-txt-counter span").text(finalCount);
//    });
    $(document).on("click", "#new-journey-edit, .choose-photo", function(e) {
        if ($("#incogo_member_journey_update_journey_description").hasClass('graytxt')) {
            $("#incogo_member_journey_update_journey_description").removeClass('graytxt');
        }
        if ($(this).hasClass('choose-photo')) {// if journey edit button is clicked
            $('.save-j-update').removeClass('is-story-update');
        } else {
            $('.save-j-update').addClass('is-story-update');
            var total = 220 - $('#incogo_member_journey_update_journey_description').val().length;
            $('.journey-txt-counter span').text(total);
        }
        $(".home-btn").hide();
        $("#main-menu").attr("data-mode", 'story');
        if ($("#can-edit-story").hasClass("can-edit") == false) {
            $("#can-edit-story").addClass("can-edit");
            $(".save-j-update").addClass("story-edit");
            $("#edit-journey-link").hide();
            showEditMenu(".edit-mode", ".default-mode"); //first argument for show and second for hide 
            $("#incogo_member_journey_update_journey_title").val($(".journey-edit-title").text());
            if ($("#detail-txt-p").text() != "Tap here to write your story. Your story explains this journey and why it's important to you.") {
                $("#incogo_member_journey_update_journey_description").val($("#detail-txt-p").text());
            }
            $("#incogo_member_journey_update_journey_description").focus();
            // $("#incogo_member_journey_update_journey_title").focus();
            $("#new-tabination-menu li").each(function() {
                if ($(this).hasClass('active') == false) {
                    $(this).addClass('stop-tab-switch');
                }
            });

        }
    });


    $(document).on("submit", "#frm_journey_update", function(e) {
        e.preventDefault();
        uiBlocker();
        var url = $("#frm_journey_update").attr('action');
        $.post(url, $("#frm_journey_update").serialize(), function(response) {
            uiUnBlocker();
            if (response.Good) {
                var editCondition = $("#main-menu").attr('data-mode');
                $('.incogo-me-1 .incogo-me-header-img-wrap').attr('data-img', response.src);
                $(".update-story").removeClass("update-story");
                $(".stop-tab-switch").removeClass('stop-tab-switch');
                $("#can-edit-story").removeClass("can-edit");
                $(".save-j-update").removeClass("story-edit");
                $("#edit-journey-link").show();
                $(".home-btn").show();

                $(".journey-edit-title").text($("#incogo_member_journey_update_journey_title").val());
                
                var jurl = $("#linkarea").attr('data-jUrl');
                var jmessage = $("#linkarea").attr('data-jmessage');
                var newJmessage = jmessage.replace("title", $("#incogo_member_journey_update_journey_title").val());
                newJmessage = newJmessage.replace("url", jurl);
                $("#linkarea").val(newJmessage);
                $("#linkarea").attr('data-placeholder', newJmessage);


                if ($("#incogo_member_journey_update_journey_description").val() == '') {
                    $("#detail-txt-p").text("Tap here to write your story. Your story explains this journey and why it's important to you.");
                } else {
                    var newStr = replaceURL($("#incogo_member_journey_update_journey_description").val());
                    newStr = $.parseHTML( newStr );
                    $("#detail-txt-p").html(newStr);
                }
                $(".journey-name").text($("#incogo_member_journey_update_journey_title").val());
                $(".new-support-heading").text($("#incogo_member_journey_update_journey_title").val());
                showEditMenu(".default-mode", ".edit-mode"); //first argument for show and second for hide 


                if (response.hasOwnProperty('src') && isMobile.AndroidApp()) {
                    //showMessage('This image has been saved!');
                    newMessage('This image has<br>been saved!');
                    setImageSrc(response.src, 'save');
                    $("#fbShare").attr("data-img", response.src);
                } else {
                    //showMessage('Your story has been saved!');
                    newMessage('Your story has<br>been saved!');
                    if( $('#upload-j-photo-promt').hasClass('upload-j-image')){
                       $('.tabination-menu li').first().find("a").trigger("click");
                       $('#upload-j-photo-promt').fadeIn();
                       $('#write-j-story-promt').remove();
                    }
                }


                $("#main-menu").attr('data-mode', '');
                //update facebook shares 
                if ($("#incogo_member_journey_update_journey_description").val() != '') {
                    $("#fbShare").attr("data-desc", $("#incogo_member_journey_update_journey_description").val());
                }
                if ($("#incogo_member_journey_update_journey_title").val() != '') {
                    $("#fbShare").attr("data-title", $("#incogo_member_journey_update_journey_title").val());
                }


            }
            if (response.Errors) {
                genericDialogue("Exceeded Character Limit", "You have exceeded the character limit. Journey titles are to be kept to 23 characters and journey stories are to be no more than 220 characters.");

            }
        });
        return false;
    });
    var isFileAvailable = false;
    var LastFileName = false;
    var imageSelected = false;
    isFileUpload = 'no';
    var jqXHR = "";
    if ($("#journey-update-image").attr("src") != "" && $("#journey-update-image").attr("src") != "/incogo/web/bundles/incogomember/images/journey_bg_img.jpg") {
        imageSelected = true;
    }


//    $('#frm_journey_update').fileupload({
//        dropZone: null,
//        singleFileUploads: false,
//        limitMultiFileUploads: false,
//        disableImageMetaDataSave: true, // Otherwise orientation is broken on iOS Safari
//        imageOrientation: true,
//        // previewOrientation: 0, //If imageOrientation true this only works on iOS
//        // previewOrientation: true, //If imageOrientation true this only works on NON-iOS
//        previewOrientation: navigator.userAgent.match(/(iPad|iPhone|iPod touch);.*CPU.*OS 7_\d/i) ? 0 : true,
//        previewMaxWidth: 100,
//        previewMaxHeight: 100,
//        previewCrop: true,
//        add: function(e, data) {
//
//            jqXHR = data;
//            isFileUpload = 'yes';
//            var uploadFile = data.files[0];
//            if (uploadFile.name) {
//                if (!(/\.(gif|jpg|jpeg|png)$/i).test(uploadFile.name)) {
//                    isFileUpload = 'no';
//                    jqXHR.abort();
//                    genericDialogue('Unrecognised file', 'Sorry, the file you have selected is not recognized. Please select another file.');
//                    $(".save-j-update").removeClass('update-story');
//                    return false;
//                }
//            }
//            if (uploadFile.size > 10485760) { // 10mb
//                jqXHR.abort();
//                isFileUpload = 'no';
//                showDialogue('Invalid file size', 'Please upload a smaller image, max size is 10 MB');
//                $(".save-j-update").removeClass('update-story');
//                return false;
//            }
//            // show image thumbnail before upload 
//            var image_thumb = "";
//            if (data.files && data.files[0]) {
//                var reader = new FileReader();
//                reader.onload = function(e) {
//                    setImageSrc(e.target.result, 'preview');
//                }
//                reader.readAsDataURL(data.files[0]);
//            }
//            setTimeout(function() {
//                imageResize(".journey-image-holder img");
//            }, 100);
//            data.context = $(document).on("click", ".save-j-update", function() {
//
//                if ($(this).hasClass('is-story-update')) {
//                    jqXHR.abort();
//                    return false;
//                }
//
//                if (isFileUpload == 'yes') {
//                    data.submit();
//                    uiBlocker();
//                }
//            });
//            imageSelected = true;
//        },
//        change: function(e, data) {
//            isFileUpload = 'yes';
//            isUploadNew = true;
//            resetFormSaveBinding();
//            if (isValidFile(data.files[0].type)) {
//                isFileAvailable = true;
//                LastFileName = data.files[0].name;
//            } else {
//                isFileAvailable = false;
//                //resetFormSaveBinding();
//            }
//        },
//        progress: function(e, data) {
//        },
//        fail: function(e, data) {
//            uiUnBlocker();
//            resetFormSaveBinding();
//
//        },
//        done: function(e, data) {
//            uiUnBlocker();
//            var result = data.result;
//            if (result.Good) {
//                
//                //showMessage('This image has been saved!');
//                newMessage('This image has<br>been saved!');
//                setImageSrc(result.src, 'save');
//                $("#fbShare").attr("data-img", result.src);
//                $(".home-btn").show();
//
//                $(".update-story").removeClass("update-story");
//                $(".stop-tab-switch").removeClass('stop-tab-switch');
//                $("#can-edit-story").removeClass("can-edit");
//                $(".save-j-update").removeClass("story-edit");
//                $("#edit-journey-link").show();
//                isFileUpload = 'no';
//
//                //$(".journey-edit-title").text($("#incogo_member_journey_update_journey_title").val());
////                if ($("#incogo_member_journey_update_journey_description").val() != '') {
////                    $("#detail-txt-p").text($("#incogo_member_journey_update_journey_description").val());
////                }
//                showEditMenu(".default-mode", ".edit-mode"); //first argument for show and second for hide 
//                //resetFormSaveBinding();
//                // need to discuss with tahir
////                setTimeout(function() {
////                    imageResize(".incogo-me-1 .incogo-me-header-img-wrap");
////                }, 100);
//                if( $('#write-j-story-promt').hasClass('write-j-story')){
//                    $(".tabination-menu li:eq( 1 )").find("a").trigger("click");
//                    $('#write-j-story-promt').fadeIn();
//                    $('#upload-j-photo-promt').remove();
//                    
//                 }
//            }
//            else {
//                uiUnBlocker();
//            }
//        }
//    });
    $(document).on("click", "#cancel-j-update", function(e) {
        $(".home-btn").show();
        $('.save-j-update').removeClass('is-story-update');
        $('#incogo_member_journey_update_journey_title').removeClass('error');
        if (jqXHR) {
            jqXHR.abort();
        }

        isFileUpload = 'no';
        $("#incogo_member_journey_update_journey_title").val($(".journey-edit-title").text());
        if ($("#incogo_member_journey_update_journey_description").val() != '' && $("#incogo_member_journey_update_journey_description").val() != "Tap here to write your story. Your story explains this journey and why it's important to you.") {
            $("#incogo_member_journey_update_journey_description").val($("#detail-txt-p").text());
        }
        if ($(".save-j-update").hasClass("story-edit") == true) {   //story edit mode cancel  
            $("#can-edit-story").removeClass("can-edit");
            $(".save-j-update").removeClass("story-edit");
            $("#edit-journey-link").show();
            showEditMenu(".default-mode", ".edit-mode"); //first argument for show and second for hide 
            setImageSrc('', 'cancel');
            $("#androidImage").remove();
            $(".stop-tab-switch").removeClass('stop-tab-switch');
            $("#editErrorMessage").hide();
        }
    });
    $(document).on('drop dragover', function(e) {
        e.preventDefault();
    });
    $(document).on("click", "#canecl-edit-journey", function() {
        $("#edit-journey-link").show();
    });

    $(document).on("click", "[data-popups-alert]", function(e) {
        e.preventDefault();
        var showMe = $(this).attr("href");
        if ($(showMe).is(":visible") == false) {
            $(showMe).fadeIn();
        }
        var my = document.getElementById('linkarea');
        var input = my;
        input.focus();
        input.select();
        $(document).on("focus", "#linkarea", function() {
            setTimeout(function() {
                input.select();
            }, 100);
        });
    });

//    $(document).on("click", ".cancel-popups", function(e) {
//        e.preventDefault();
//        if ($("#linkarea").is(":visible")) {
//            $("#linkarea").blur();
//            $(".journey-msg").fadeOut(function() {
//                $("html, body").scrollTop(0);
//            });
//        } else {
//            $(".journey-msg").fadeOut(function() {
//                $("#next-gen-popup").remove();
//                $("#genericDialog").remove();
//                $("#tabPop").remove();
//            });
//        }
//    });
    $(document).on("click", ".close-popups", function(e) {
        e.preventDefault();
        if ($("#genericDialog").is(":visible")) {
            $("#genericDialog").fadeOut(function() {
                $("#genericDialog").remove();
            });
        }
    });
    /************************************* Archives Journey ********************************/
    $(document).on("click", "#btnArchiveJourney", function() {
        uiBlocker();
        var ref = $(this).attr('data-ref');
        var url = BASEPATH + 'member/setjourneystatus/archive/' + ref
        $.post(url, {}, function(response) {
            if (response.Good) {
                triggerGa('Journey', 'click', 'Archive journey');
                window.location.href = response.redirect;
            } else {
                uiUnBlocker();
            }
        }).error(function() {
            uiUnBlocker();
        });
    });

    /*************************************  save pop up journey ********************************/
    $(document).on("click", ".save-journey-popups", function() {
        $("#tabPop").fadeOut();
        if ($(".save-j-update").is(":visible") == true) {
            $(".save-j-update").trigger("click");
        } else if ($(".save-supporter").is(":visible") == true) {
            $(".save-supporter").trigger("click");
        }
        $("#tabPop").remove();
    });

    /*************************************  Start the completed journey again  ********************************/
    $(document).on("click", "#btnStartJourney", function() {
        uiBlocker();
        var ref = $(this).attr('data-ref');
        var url = BASEPATH + 'member/setjourneystatus/start/' + ref
        $.post(url, {}, function(response) {
            if (response != '0') {
//                $(".complete-j-link").fadeIn();
//                $(".support-j-link").fadeIn();
//                $(".archive-j-link").fadeOut();
//                $(".start-journey-again").fadeOut();
//                $("#archiveJourney").fadeOut();
//                $("#completeJourney").fadeOut();
//                $("#startJourney").fadeOut();
//                uiUnBlocker();
//                if (response.redirect) {
//                    triggerGa('Journey', 'click', 'Reactivate journey');
//                    location.reload();
//                }
                window.location.href = response.redirect;
                triggerGa('Journey', 'click', 'Reactivate journey');
                //$("#page-template").html(response);
            }
        });
    });
    /*************************************  Journey Compelte ********************************/
    $(document).on("click", "#btnCompleteJourney", function() {
        uiBlocker();
        var ref = $(this).attr('data-ref');
        //var url = BASEPATH + 'member/setjourneystatus/complete/' + ref
        var url = BASEPATH + 'member/setjourneystatus/complete-archive/' + ref
        $.post(url, {}, function(response) {
            if (response != '0') {
               $(".incogo-me-header-img-wrap").removeClass('new-image');
               $(".journey-inv-btn").remove();
               $(".post-wrapper").remove();
               $(".upload-n-j-img").remove();
               $("#completeJourney").fadeOut();
               $(".complete-j-link").fadeOut();
               $(".support-j-link").fadeOut();
//                $(".archive-j-link").fadeIn();
//                $("#archiveJourney").fadeIn();
               $(".start-journey-again").fadeIn();
                triggerGa('Journey', 'click', 'Complete journey');
                window.location.href = response.redirect;
                //uiUnBlocker();
//                if (response.redirect) {
//                    window.location = response.redirect;
//                }
                //$("#page-template").html(response);
            }
        });
    });
    /*************************************  Delete Compelte ********************************/
    $(document).on("click", "#btnDeleteJourney", function() {
        uiBlocker();
        var ref = $(this).attr('data-ref');
        var redir = $(this).attr('data-from');
        var url = BASEPATH + 'member/deletejourney/' + ref + '/'+redir
        $.post(url, {}, function(response) {
            if (response.Good) {
                triggerGa('Journey', 'click', 'Delete journey');
                window.location.href = response.redirect;
            }
        }).error(function() {
            uiUnBlocker();
        });
    });
    /************************************* unsupport journey ********************************/

    $(document).on("click", ".killSupport", function() {
        uiBlocker();
        var url = BASEPATH + 'member/killsupport/' + $(this).attr('data-ref');
        $.post(url, {subEvent:'journey box'}, function(response) {
            if (response.Good) {
                if ($(this).hasClass("support-btn-wrap") == true) {
                    $(this).hide();
                }
                window.location.href = response.redirect;
            } else {
                uiUnBlocker();
            }
        }).error(function() {
            uiUnBlocker();
        });
    });
    $(document).on("click", ".addSupport", function() {
        uiBlocker();
        var url = BASEPATH + 'member/joinsupporters/' + $(this).attr('data-ref');
        $.post(url, {subEvent:'journey box'}, function(response) {
            if (response.Good) {
                triggerGa('Support journeys', 'Click', 'Support journey from journeybox');
                location.reload();
            } else {
                uiUnBlocker();
            }
        }).error(function() {
            uiUnBlocker();
        });
    });
    $(document).on("click", ".nologin-support", function() {
        uiBlocker();
        var url = BASEPATH + 'member/setautosupport/' + $(this).attr('data-ref') + '/autojoin';
        $.post(url, {}, function(response) {
            if (response.Good) {
                if(response.showPopup){
                     uiUnBlocker();
                    $('#sign-in-overlay-jbox').show();
                }else{
                    window.location.href = response.redirect;
                }
            } else {
                uiUnBlocker();
            }
        }).error(function() {
            uiUnBlocker();
        });
    });
    $(document).on("click", ".unsupport-box", function(e) {
        e.stopPropagation();
        $(".unsupport-j-link").trigger("click");
    });
    $(document).on("click", ".support-j-link", function() {
        $(".default-mode").each(function() {
            $(this).hide();
        });
        $(".home-btn").hide();
        $(".acMemberListContainer li").each(function() {
            $(this).removeAttr("data-redirect-non-t");

            $(this).find("a.delete-friend-btn").fadeIn();
        });

        $(".journey_update_mode").each(function() {
            $(this).show();
        });
        $('#main-menu .journey_update_mode:first-child').addClass('cancel-supporter');
        $('.save-j-update').addClass('save-supporter');
        $('.save-j-update').attr('save-supporter');


        $(".journey_update_mode").removeClass("edit-mode ,story-edit, save-j-update");
        $(".journey_update_mode").removeClass("story-edit");
        $(".journey_update_mode").removeAttr("id");

        //if ($("#new-tabination-menu li.down-ticker").hasClass("active")) {
        // $(".acMemberListContainer li a.delete-friend-btn").fadeIn();
        //}
        $("#new-tabination-menu li a ").each(function() {
            if ($(this).attr("data-rel") == 2) {
                $(this).trigger("click");
            }
        });
        $("#new-tabination-menu li").each(function() {
            if ($(this).hasClass('active') == false) {
                $(this).addClass('stop-tab-switch');
            }
        });

    });
    $(document).on("click", ".cancel-supporter", function() {
        $(".home-btn").show();

        $(".acMemberListContainer li").each(function() {
            $(this).find("a.delete-friend-btn").fadeOut();
            $(this).removeClass('grayed');
            $(this).attr("data-redirect-non-t", $(this).attr("data-link"));
        });

        $(".journey_update_mode").each(function() {
            $(this).hide();
        });
        $(".default-mode").each(function() {
            $(this).show();
        });
        $('#main-menu .journey_update_mode:first-child').removeClass('cancel-supporter');

        $('.save-supporter').addClass('save-j-update');
        $('.journey_update_mode').removeClass('save-supporter');


        $(".journey_update_mode").addClass("edit-mode");
        $(".journey_update_mode").addClass("story-edit");
        $(".journey_update_mode:first-child").attr("id", "cancel-j-update");

        $(".acMemberListContainer li a.delete-friend-btn").fadeOut();
        $("#new-tabination-menu li").each(function() {
            $(this).removeClass('stop-tab-switch');
        });

    });
    $(document).on("click", ".acMemberListContainer li a.delete-friend-btn", function() {

        $(this).parent('li').addClass("grayed");
    });
    $(document).on("click", ".save-supporter", function() {
        if ($(".acMemberListContainer li").hasClass("grayed")) {
            $("#deleteSupporter").show();
        }
    });
    $(document).on("click", ".save-my-supporter", function() {
        var userblunder = 0;
        $(".acMemberListContainer li").each(function() {
            if ($(this).hasClass("grayed") == true) {
                userblunder = userblunder + 1;
            }
        });
        if (userblunder == 0) {
            return false;
        }
        var totalCount = parseInt($(".down-ticker span.nums").text()) - parseInt($(".acMemberListContainer li.grayed").length);

        var url = BASEPATH;
        $(".acMemberListContainer li a.delete-friend-btn").fadeOut();
        var deletedItems = "";
        $(".acMemberListContainer li.grayed .delete-friend-btn").each(function() {
            if (deletedItems == "") {
                deletedItems = $(this).attr('data-ref');
            } else {
                deletedItems += "," + $(this).attr('data-ref');
            }
        });
        $(".acMemberListContainer li.grayed").remove();
        if ($(this).attr('data-view') == 'incogoNetwork' && deletedItems != "") {
            url += 'member/deletefriend';
            $.post(url, {friends: deletedItems}, function(response) {
                $(".cancel-popups").trigger("click");
                $(".cancel-incogonetwork").trigger("click");
                triggerGa('Personal network', 'click', 'Delete friend')
                showMessage('The selected members<br> have been deleted.');

            });

        } else {
            if (deletedItems != "") {
                uiBlocker();
                url += 'member/deletesupporters/' + $(".save-my-supporter").attr("data-ref");
                $.post(url, {supporters: deletedItems}, function(response) {
                    if (totalCount < 0) {
                        totalCount = 0;
                    }
                    $(".home-btn").hide();
                    $(".down-ticker span.nums").text(totalCount);
                    $(".cancel-popups").trigger("click");
                    $(".cancel-supporter").trigger("click");
                    //showMessage('Your supporters have been saved!');
                    newMessage('Your supporters<br>have been saved!');
                    uiUnBlocker();
                }).always(function() {
                    uiUnBlocker();
                });
//                
//                if ($(this).attr("data-view") == "journeySupporters") {
//                    if (!$("#journeyRef").length || isNaN($("#journeyRef").val())) {
//                        return false;
//                    }
//                    url += 'member/deletesupporters/' + $("#journeyRef").val();
//                    $.post(url, {supporters: deletedItems}, function(response) {
//                    });
//                } else {
//                    url += 'member/deletefriend';
//                    $.post(url, {friends: deletedItems}, function(response) {
//                    });
//                }


            }
        }


    });
    $(document).on('keyup keydown paste change input focus', ".edit-j-textarea", function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
        var length = $(this).val().length;
        var totalChar = 220;
        var finalCount = totalChar - length;
        $(".journey-txt-counter span").text(finalCount);
        if (length > 220) {
            finalCount = 0;
            $(".journey-txt-counter span").text(finalCount);
            $("p.journey-txt-counter").addClass('red');
            $("#incogo_member_journey_update_journey_description").val($("#incogo_member_journey_update_journey_description").val().slice(0, 220));
            e.preventDefault();
            return false;
        }
        $(".journey-txt-counter span").text(finalCount);

        if (finalCount === 0) {
            $("p.journey-txt-counter").addClass('red');
        } else {
            $("p.journey-txt-counter").removeClass('red');
        }

    });
    $(document).on("click", ".choose-photo", function() {

        if (isMobile.AndroidApp()) {
            window.JSInterface.autoRefreshStatus(false);
            window.JSInterface.loadFilechooser("journey");
        } else {
            $("#incogo_member_journey_update_image_file").trigger("click");
            $("#main-menu").attr("data-mode", 'image');

        }


    });
    $(document).on("keypress", "#incogo_member_journey_update_journey_title", function(e) {
        if (e.keyCode === 13) {
            $(".save-j-update").trigger("click");
            return false;
        }
    });
    /*********************Like Post************************/

//    $(document).on("click", ".like-btn", function(e) {
//        return false;
//        e.preventDefault();
//        var me = $(this);
//        if ($(this).hasClass('showpopup') == true) {
//            $("#acsupportbar").attr("data-action", "support");
//            $("#acsupportbar").fadeIn();
//            $('.like-btn').removeClass('post-popup-like');
//            $(this).addClass('post-popup-like');
//            me.addClass("reload");
//            return false;
//        } else {
//            uiBlocker();
//        }
//        if (me.hasClass('blocked') == true) {
//            return false;
//        }
//        me.addClass('blocked');
//        var me = $(this);
//        var ref = me.attr('data-ref');
//        var url = BASEPATH + 'member/likepost/' + ref;
//        var op = '+';
//        if (me.hasClass('liked')) {
//            url = BASEPATH + 'member/unlikepost/' + ref;
//            op = '-';
//        }
//
//        $.post(url, {}, function(response) {
//            if (response.Good) {
//                //if ($(".post_like_counter").length) {
//                if ($('.post_like_counter[data-ref=' + ref + ']').length) {
//
//                    var myCounter = $('.post_like_counter[data-ref=' + ref + ']');
//                    var count = parseInt(myCounter.attr('data-count'));
//                    if (op === '+') {
//                        count += 1;
//                        myCounter.attr('data-count', count);
//                        me.addClass('liked');
//                        if (count == 1) {
////myCounter.html('Me');
//                            myCounter.html('1')
//                            myCounter.next().html(" Support");
//                        } else {
//                            myCounter.next().html(" Supports");
//                            myCounter.html(count);
//                        }
//                    } else {
//                        count -= 1;
//                        myCounter.attr('data-count', count);
//                        if (count == 0) {
//                            myCounter.html('');
//                            myCounter.next().html(" Support");
//                        } else {
//                            if (count == 1) {
//                                myCounter.next().html(" Support");
//                            } else {
//                                myCounter.next().html(" Supports");
//                            }
//                            myCounter.html(count);
//                        }
//                        me.removeClass('liked');
//                    }
//                }
//            }
//        }).always(function() {
//            me.removeClass('blocked');
//            if (me.hasClass("reload")) {
//                location.reload();
//            }
//            uiUnBlocker();
//        });
//    });
    /******************************select invite *********************************/
    $(document).on("click", ".invite-candidate", function() {
        if ($(".processInvites").hasClass("processing")) {
            return false;
        }
        if ($(this).hasClass("invited") || $(this).hasClass("supporting")) {
            return false;
        }
        $(this).toggleClass("selected");

    });
    $(document).on("click", "#checkbox1", function() {

        if ($(".processInvites").hasClass("processing")) {
            return false;
        }
        if ($("#invite-supporter-menu li:eq(0)").hasClass("active") == true) {
            if ($(this).hasClass("checked") == false) {
                $(this).addClass("checked");
                $("#tab-1 .acMemberListContainer li").each(function() {
                    if ($(this).hasClass("invited") == false && $(this).hasClass("supporting") == false) {
                        $(this).addClass("selected");
                    }
                });
            } else {
                $(this).removeClass("checked");
                $("#tab-1 .acMemberListContainer li").each(function() {
                    $(this).removeClass("selected");
                });
            }
        }


    });
    $(document).on("click", "#checkbox2", function() {

        if ($(".processInvites").hasClass("processing")) {
            return false;
        }

        if ($(this).hasClass("checked") == false) {
            $(this).addClass("checked");
            $("#tab-2 .acMemberListContainer li").each(function() {
                if ($(this).hasClass("invited") == false && $(this).hasClass("supporting") == false) {
                    $(this).addClass("selected");
                }
            });
        } else {
            $(this).removeClass("checked");
            $("#tab-2 .acMemberListContainer li").each(function() {
                $(this).removeClass("selected");
            });
        }



    });

    $(document).on('click', '.processInvites', function(e) {        
        
        if (!$("li.inviteme").length) {
            return false;
        }

        if (parseInt($(".acMemberListContainer li").length) <= 0 || $(".acMemberListContainer li").length <= '0') {
            return false;
        }
        if ($(this).hasClass('processing')) {
            return false;
        }

        $(this).addClass('processing');


        var inviteList = '';
        $('li.inviteme').each(function() {
            inviteList += $(this).attr('data-ref') + ',';
        });
        var url =  $(this).attr('data-url'); 
        uiBlocker();
        iosKeyfix(function(){
            $.post(url, {invites: inviteList}, function(response) {
                if (response.Good) {
                    //showMessage('Invitation has been sent!');
                    window.history.back();
                    return false;
                }
            }).always(function() {
                $('.processInvites').removeClass('processing');
                uiUnBlocker();
            });
        },1000);

    });
    $(document).on("click", ".edit-incogonetwork", function() {
        $(".acMemberListContainer li").each(function() {
            $(this).removeAttr("data-redirect-non-t");
            $(this).find("a.delete-friend-btn").fadeIn();
        });
        $(".back-btn").addClass("hidden");
        $(".edit-incogonetwork").addClass("hidden");
        $(".cancel-incogonetwork").removeClass("hidden");
        $(".del-incogonetwork").removeClass("hidden");
    });
    $(document).on("click", ".cancel-incogonetwork", function() {
        $(".acMemberListContainer li").each(function() {
            $(this).removeClass("grayed");
            $(this).attr("data-redirect-non-t", $(this).attr("data-link"));
            $(this).find("a.delete-friend-btn").fadeOut();
        });
        $(".back-btn").removeClass("hidden");
        $(".edit-incogonetwork").removeClass("hidden");
        $(".cancel-incogonetwork").addClass("hidden");
        $(".del-incogonetwork").addClass("hidden");
    });
    $(document).on("click", ".del-incogonetwork", function() {
        var total = 0;
        $(".acMemberListContainer li").each(function() {
            if ($(this).hasClass('grayed') == true) {
                total = total + 1;
            }
        });
        if (total > 0) {
            $("#deleteMembers").show();
        }
    });

    $(document).on("change cut", "#linkarea", function() {
        var placeholder = $(this).attr("data-placeholder");
        $(this).val(placeholder);
    });

});
// facebook twitter share code start 
//twitter share stuff

// fb share stuff

var fb_app_id = '179611298840678';
$(document).ready(function() {
    $(document).off("click", "#twShare");
    $("#twShare").unbind("click");
    $('#twShare').click(function() {
        twShare("Support the journey "+$(this).attr("data-title")+" on Incogo", $(this).attr("data-url"));
        triggerGa('Journey', 'click', 'Twitter share');
    });
    $('#fbShare').click(function() {
        var jouneyMaker = $(this).attr("data-name");
        var jouneydescription = $(this).attr("data-desc");
        var fbTtitle = $(this).attr("data-title");
        var journeyUrl = $(this).attr("data-url");
        var fbImg = $(this).attr("data-img");
        if (fbImg.indexOf('http') == -1) {
            fbImg = 'http:' + fbImg;
        }
        fbShare(journeyUrl, fbTtitle, fbImg, jouneyMaker, jouneydescription);
        triggerGa('Journey', 'click', 'Facebook share');

    });
});
// facebook twitter share code end 
////////////////////////////////////////////////////////////////////////////////////////////////////
function isValidFile(fileType) {
    return fileType == "image/jpeg" || fileType == "image/gif" || fileType == "image/png";
}
function blurTabImage(obj) {
    if ($(obj).is(":visible")) {
        $(obj).pixastic("blurfast", {amount: 2});
    }
}
function whichTabClass(num, tabs) {
    $("#acincogo-journey").removeAttr("class");
    $("#acincogo-journey").addClass("row incogo-me-header-" + num);
    if (tabs == "1") { 
        if ($("#new-journey-tab").is(":visible")) {
               if($(".jsupporters").attr('data-loaded')==0){
                 $(".jsupporters").attr('data-loaded',1)
                  uiBlocker();
//                    var flashCacheBreaker = new Date().getTime();
//                    var jsjourneyId = $('.jsupporters').attr('data-journeyId');
//                     $.post(BASEPATH + 'member/journeysupporters/' + jsjourneyId + '/1/1?v=' + flashCacheBreaker, {}, function (response) {
//                          uiUnBlocker();
//                         $('#new-supporters-tab').html(response);
//                     });  
                      
                        getJourneySupportersByPage(1,1);
               }
              
            $("#new-journey-tab").hide();
            $("#new-supporters-tab, #supporter-new-search").show();
        }
    } else {
        if ($("#new-supporters-tab").is(":visible")) {
            $("#new-supporters-tab, #supporter-new-search").hide();
            $("#new-journey-tab").show();
        }
    }
    if (num == "2") {
        if ($(".blur-me-2").is(":visible") == false) {
            //$("#detail-grey-text p, #detail-grey-text textarea").addClass("graytxt");
            $("#detail-grey-text").css({"display": "block"});
        } else {
            $("#detail-grey-text p, #detail-grey-text textarea").removeClass("graytxt");
        }
    }
    journey_scroller();

}
function journey_scroller() {
    if(isMobile.any()){
        $('html').addClass('mobs');
    }
    var howMuch = 245;
    if ($(window).width() < 480) {
        howMuch -= 60;
    }
    if ($(document).scrollTop() > howMuch) {
        if(!isMobile.any()){
            $("#journey-fixed-post").addClass("fixed-post");
        }else {
            $(".tabination-menu-support").hide();
        }
        if ($("#acincogo-journey").hasClass("incogo-me-header-1")) {
            $("#incogo-handler").removeAttr("class");
            $("#incogo-handler").addClass("fixer fixed-1");
        }
        if ($("#acincogo-journey").hasClass("incogo-me-header-2")) {
            $("#incogo-handler").removeAttr("class");
            $("#incogo-handler").addClass("fixer fixed-2");
            if (blured2 == false) {
                if (isUploadNew) {
                    $(".blur-me-3").load(function() {
                        blurTabImage(".blur-me-3");
                        blured2 = true;
                        isUploadNew = false;
                    });
                } else {
                    blurTabImage(".blur-me-3");
                    blured2 = true;
                }
            }
        }
        if ($("#acincogo-journey").hasClass("incogo-me-header-3")) {
            $("#incogo-handler").removeAttr("class");
            $("#incogo-handler").addClass("fixer fixed-3");
        }
    } else {
        $("#incogo-handler").removeAttr("class");
        $("#journey-fixed-post").removeClass("fixed-post");
        $("#incogo-handler").css({"height": $("#acincogo-journey").height() + "px"});
        $(".tabination-wrap").show();
    }
    if ($(document).scrollTop() < 200) {
        if ($("#acincogo-journey").hasClass("incogo-me-header-2")) {
            if (scrollblur == false) {
                blurTabImage(".blur-me-2");
                scrollblur = true;
            }
        }
    }

}

function showMessage(msg, timeout) {
    var time = 3000; // Default fadeOut time.
    var output = '<div id="temp-msg" class="black-message" style="display:none;"><p>' + msg + '</p></div>';
    if (timeout !== undefined) {
        time = timeout;
    }
    $('body').append(output);
    $("#temp-msg").fadeIn('fast');
    setTimeout(function() {
        $("#temp-msg").fadeOut(function() {
            $("#temp-msg").remove();
        });
    }, time);
}



function twShare(message, uri) {
    var width = 575,
            height = 400,
            left = ($(window).width() - width) / 2,
            top = ($(window).height() - height) / 2,
            opts = 'status=1' +
            ',width=' + width +
            ',height=' + height +
            ',top=' + top +
            ',left=' + left;

    //ignoring app for now
    externalHeader = 'Share';
    if (isMobile.AndroidApp()) {
        //externalLink = 'https://twitter.com/intent/tweet?text=' + encodeURIComponent(message) + '&url=' + uri, 'twitter', opts;
        //onLinkClicked();// call this for iOS

        window.JSInterface.startVideo('https://twitter.com/intent/tweet?text=' + encodeURIComponent(message) + '&url=' + uri, 'Share', 'false');// cant send paramters other than url
    } else if (isMobile.iOSApp()) {
        externalLink = 'https://twitter.com/intent/tweet?text=' + encodeURIComponent(message) + '&url=' + uri;
        onLinkClicked();
    } else {
        window.open('https://twitter.com/intent/tweet?text=' + encodeURIComponent(message) + '&url=' + uri, 'twitter', opts);
    }
}

function fbShare(url, name, image, caption, description, element) {
    var redirect_uri = $("#fbShare").attr('data-redirecturi');
    //var temp = "https://www.facebook.com/dialog/feed?app_id=" + fb_app_id + "&display=popup&name=" + encodeURIComponent(name) + "&description=" + encodeURIComponent(description) + "&picture=" + encodeURIComponent(image) + "&caption=" + encodeURIComponent(caption) + "&link=" + encodeURIComponent(url) + "&redirect_uri=" + encodeURIComponent("{{url('member_journey_journeydetail',{'id':journey.Id})}}");
    var temp = "https://www.facebook.com/dialog/feed?app_id=" + fb_app_id + "&display=popup&name=" + encodeURIComponent(name) + "&description=" + encodeURIComponent(description) + "&picture=" + encodeURIComponent(image) + "&caption=" + encodeURIComponent(caption) + "&link=" + encodeURIComponent(url) + "&redirect_uri=" + encodeURIComponent(redirect_uri);

    externalHeader = 'Share';
    //ignoring app for now
    if (isMobile.AndroidApp()) {
    //    window.JSInterface.startVideo(temp);
        window.JSInterface.startVideo(temp,'Share','false');
    } else if (isMobile.iOSApp()) {
        externalLink = temp;
        onLinkClicked();
    }
    else {
        window.open(temp)
        return true;
    }

}

function showEditMenu(show, hide) {
    $(show).each(function() {
        $(this).show();
    });
    $(hide).each(function() {
        $(this).hide();
    });

}
function formatFileSize(bytes) {
    if (typeof bytes !== 'number') {
        return '';
    }

    if (bytes >= 1000000000) {
        return (bytes / 1000000000).toFixed(2) + ' GB';
    }

    if (bytes >= 1000000) {
        return (bytes / 1000000).toFixed(2) + ' MB';
    }
    return (bytes / 1000).toFixed(2) + ' KB';
}


function setImageSrc(src, scenario) {
    switch (scenario) {
        case 'preview':
//            if(!isMobile.any() ) {
            $('.incogo-me-1 .incogo-me-header-img-wrap').css('background-image', 'url(' + src + ')');// set image for journey tab
//            }else {
            var getWidth = $("#getPreview").width();
            var getHeight = $("#getPreview").height();
            var viewWidth = getWidth;
            var viewHeight = getHeight;
            var backPos = "top left";
            var viewTop = 0;
            var viewLeft = 0;
            var isOre = '';
            var angle = '0';
            var image = new Image();
            image.src = src;
            image.onload = function() {
                EXIF.getData(image, function() {
                    isOre = EXIF.getTag(this, "Orientation");
//                        alert(isOre);
                    if (isOre == 3 || isOre == 6 || isOre == 8) {
                        if (isOre == 3) {
                            angle = '180';
                            backPos = "bottom right";
                            viewTop = getHeight;
                            viewLeft = getWidth;
                            viewWidth = getWidth;
                            viewHeight = getHeight;
                        }
                        if (isOre == 6) {
                            angle = '90';
                            backPos = "top left";
                            viewTop = 0;
                            viewLeft = getWidth;
                            viewWidth = getHeight;
                            viewHeight = getWidth;
                        }
                        if (isOre == 8) {
                            angle = '-90';
                            backPos = "bottom right";
                            viewTop = getHeight;
                            viewLeft = 0;
                            viewWidth = getHeight;
                            viewHeight = getWidth;
                        }
                        $("#inPreview").remove();
                        $('#getPreview').prepend('<div id="inPreview" class="incogo-me-header-img-preview" style="background-image:url(' + src + '); z-index:99;"></div>');
                        $("#inPreview").css({"width": viewWidth + "px", "background-position": backPos, "height": viewHeight + "px", "left": viewLeft + "px", "top": viewTop + "px", "-webkit-transform": "rotate(" + angle + "deg)", "-moz-transform": "rotate(" + angle + "deg)", "transform": "rotate(" + angle + "deg)"});
                    } else {
                        if ($("#inPreview").is(":visible")) {
                            $("#inPreview").remove();
                        }
                    }
                });
            };
//            }
            $('.new-image').removeClass("new-image");
            break;

        case 'save':
            $('.incogo-me-header-img-wrap .img-inner-wrap .incogo-me-header-img').remove();// remove previously set item

            $('.incogo-me-1 .incogo-me-header-img-wrap').attr('data-img', src);// set image for cancel scenario
            $('.incogo-me-1 .incogo-me-header-img-wrap').css('background-image', 'url(' + src + ')');// set image for journey tab
            $('.new-image').removeClass("new-image");// remove plus sign
            $('.incogo-me-2 .incogo-me-header-img-wrap').css('background-image', 'url(' + src + ')');// set image for story tab
            $('.incogo-me-header-img-wrap .img-inner-wrap img').attr('src', src); // story plus supporter tab

            if ($('.incogo-me-header-img-wrap .img-inner-wrap .incogo-me-header-img').length == 0) {// means we don't have journey image apply back new image class                
                $('.incogo-me-2 .incogo-me-header-img-wrap .img-inner-wrap').append('<img  src="' + src + '" class="incogo-me-header-img blur-me-2" />');
                $('.incogo-me-3 .incogo-me-header-img-wrap .img-inner-wrap').append('<img  src="' + src + '" class="incogo-me-header-img blur-me-3" />');
            }
            blured = false;
            blured2 = false;
            scrollblur = false;
            $("#inPreview").remove(); // for image preview handling front camera
            break;

        case 'cancel':
            if ($('.incogo-me-header-img-wrap .img-inner-wrap .incogo-me-header-img').length == 0) {// means we don't have journey image apply back new image class                
                $('.incogo-me-1 .incogo-me-header-img-wrap').addClass('new-image');
            }
            $('.incogo-me-1 .incogo-me-header-img-wrap').css('background-image', 'url(' + $('.incogo-me-1 .incogo-me-header-img-wrap').attr('data-img') + ')');// set image for journey tab
            $("#inPreview").remove(); // for image preview handling front camera
            break;
    }


}
//function showDialogue(heading, msg) {
//    var opt = '';
//    opt = '<div id="next-gen-popup" class="journey-msg" >' +
//            '<div class="journey-msg-center">' +
//            '<div class="journey-msg-box">' +
//            '<a class="modal_close" href="javascript:void(0);"></a>' +
//            '<h4>' + heading + '</h4>' +
//            '<p>' + msg + '</p>' +
//            '</div>' +
//            '</div>' +
//            '</div>';
//    $('body').append(opt);
//    $("#next-gen-popup").fadeIn('fast');
//}
//function genericDialogue(heading, msg) {
//
//    var opt = '<div id="genericDialog" class="journey-msg" >' +
//            '<div class="journey-msg-center">' +
//            '<div class="journey-msg-box">' +
//            '<h4>' + heading + '</h4>' +
//            '<p>' + msg + '</p>' +
//            '<div class="journey-msg-footer journey-msg-footer-2">' +
//            '<a class="cancel-popups last" style="width:100%;" href="javascript:void(0);">OK</a>' +
//            '</div>' +
//            '</div>' +
//            ' </div>' +
//            '</div>';
//    $('body').append(opt);
//    $("#genericDialog").fadeIn('fast');
//}
function tabSwitchPopUP(heading, msg) {

    var opt = '<div id="tabPop" class="journey-msg" >' +
            '<div class="journey-msg-center">' +
            '<div class="journey-msg-box">' +
            '<h4>' + heading + '</h4>' +
            '<p style="padding-right:27px;padding-left:27px;">' + msg + '</p>' +
            '<div class="journey-msg-footer">' +
            '<a class="cancel-popups"  href="javascript:void(0);">Cancel</a>' +
            ' <a class="save-my-supporter last save-journey-popups" href="javascript:void(0);">Save</a>' +
            '</div>' +
            '</div>' +
            ' </div>' +
            '</div>';
    $('body').append(opt);
    $("#tabPop").fadeIn('fast');
}
    