function uiBlocker(white) {
    $.blockUI({css: {
            border: 'none',
            width: '100%',
            height: '100%',
            top: '0',
            left: '0',
            backgroundColor: 'rgba(0,0,0,.7)',
            color: '#fff'
        }, baseZ: 99999, message: "<div id='spin-me' class='spinner-wrap'><div class='spinner'></div></div>",
        onBlock: function () {
            //scrollContentLoader("spin-me", 7, 3, "#fff");
        }});

}
function uiUnBlocker() {
    $.unblockUI();
}
function uiBlockDuration(duration) {
    uiBlocker();
    setTimeout(function () {
        uiUnBlocker();
    }, duration)
}



var isMobile = {
    Android: function () {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function () {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    },
    AndroidApp: function () {
        if (typeof window.JSInterface != "undefined" && typeof window.JSInterface.loadFilechooser != "undefined") {
            return true;
        }
        return false;
    },
    iOSApp: function () {
        if (checkCookie('is_mobile_app') && getCookie('is_mobile_app') == 'iOS') {
            return true;
        }
        return false;
    },
    isMobileApp: function () {
//        if (checkCookie('is_mobile_app')) {
//            return true;
//        }
        return false;
    }


};