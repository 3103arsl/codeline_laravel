'use strict';
var codeline = angular.module('codeline', [
    'ngRoute',
    'ngSanitize',
    'ngResource',
    'ngCookies',
    'infinite-scroll',
    'tagged.directives.infiniteScroll',
    'mgcrea.pullToRefresh',
]).config(function ($routeProvider, $locationProvider) {

    $routeProvider.when('/login',
            {
                controller: 'LoginController',
                templateUrl: 'views/authentication/login.html'
            }).when('/signup',
            {
                controller: 'SignupController',
                templateUrl: 'views/authentication/signup.html'
            }).when('/home',
            {
                controller: 'HomeController',
                templateUrl: 'views/home/home.html'
            }).when('/create-new',
            {
                controller: 'NewFilmsController',
                templateUrl: 'views/film/new-film.html'
            }).when('/post-comment/:id',
            {
                controller: 'CommentController',
                templateUrl: 'views/film/comment.html'
            }).when('/films',
            {
                controller: 'FilmsController',
                templateUrl: 'views/film/all.html'
            }).when('/films/:slug?',
            {
                controller: 'FilmController',
                templateUrl: 'views/film/detail.html'
            }).when('/logout', {
        controller: 'LogoutController',
        templateUrl: 'views/authentication/logout.html'
    }).otherwise({redirectTo: '/home'});
});
