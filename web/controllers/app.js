var LoggedInUser = '';
codeline.controller('AppController', function ($scope, $rootScope) {

    $scope.environement = 'local';
    $scope.BASEPATH = '';
    $scope.ImagePrefix = '';
    $scope.alerts = '';
    $rootScope.LoggedInUser = '';

    $scope.config = {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;',
            'authToken': app_.storage_get('authToken')
        }
    }

    if (app_.storage_get('authToken') == null || app_.storage_get('authToken') == 'null') {
        //window.location = '#/login';
    } else {
        //window.location = '#/home';
    }

    if ($scope.environement === 'local') {
        $scope.BASEPATH = 'http://localhost/codeline_laravel/api/v1';
    }

    $scope.__LoginUrl = $scope.BASEPATH + '/login';
    $scope.__RegisterUrl = $scope.BASEPATH + '/user';
    $scope.__LogoutUrl = $scope.BASEPATH + '/logout';
    $scope.__FilmsUrl = $scope.BASEPATH + '/films';
    $scope.__FilmUrl = $scope.BASEPATH + '/films/';
    $scope.__FilmCreateUrl = $scope.BASEPATH + '/films/save';
    $scope.__FilmCommentUrl = $scope.BASEPATH + '/films/comment/';




    $scope.isLogin = function () {
        if (app_.storage_get('authToken') == 'null' || app_.storage_get('authToken') == null) {
            window.location = '#/login';
        }
    }

    $scope.IsArray = function (data) {
        if ($.type(data) == 'array' || $.type(data) == 'object') {
            return true
        }
        return false;
    }

    $scope.getName = function () {
        return  app_.storage_get('userName');

    }
    $scope.getRole = function () {
        return app_.storage_get('userType');
    }

    $scope.generatePages = function (meta) {
        var data = [];
        for (var i = 1; i <= meta.last_page; i++) {
            data.push(i);
        }

        return data;
    }

});
