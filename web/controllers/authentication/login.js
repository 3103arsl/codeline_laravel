codeline.controller('LoginController', function ($scope, $rootScope, $http, $route) {
    $scope.isLogin();
    var user, username, password, pushToken, deviceType;
    $scope.username = '3103arsl@gmail.com';
    $scope.password = 'member321';

    $scope.submit = function () {
        if (!$('#login-form').validationEngine('validate')) {
            return false;
        }
        if ($scope.username && $scope.password) {
            $scope.sendData();
        } else {
            $scope.alerts = 'Username or password required.';
            $scope.$apply();
            $("#error-alert").removeClass('hidden');
            uiUnBlocker();
        }
    }


    $scope.sendData = function () {
        uiBlocker();
        var data = $.param({
            email: $scope.username,
            password: $scope.password,
            platform: $scope.deviceType,
            push_token: $scope.pushToken
        });

        $http.post($scope.__LoginUrl, data, $scope.config)
                .success(function (response, status, headers, config) {
                    uiUnBlocker();
                    if (response.Good) {
                        app_.storage_set('authToken', response.authToken);
                        app_.storage_set('userName', response.user.name);
                        app_.storage_set('userType', response.user.user_type);
                        $rootScope.userName = response.user.name;
                        window.location = '#/films';

                    } else {
                        $scope.alerts = response.errors;
                        $scope.$apply();
                        $("#error-alert").removeClass('hidden');
                    }
                })
                .error(function (response, status, header, config) {
                    uiBlocker();
                });
    };
});
