codeline.controller('LogoutController', function ($scope, $rootScope, $http) {
    //Check is user login
    $scope.isLogin();

    uiBlocker();
    var data = $.param({});
    $http.post($scope.__LogoutUrl, data, $scope.config)
            .success(function (response, status, headers, config) {
                uiUnBlocker();
                if (response.Good) {
                    app_.storage_set('authToken', null);
                    app_.storage_set('userName', null);
                    app_.storage_set('userType', null);
                    window.location = '#/login';
                }
            })
            .error(function (response, status, header, config) {
                uiBlocker();
            });

});
