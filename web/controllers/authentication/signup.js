codeline.controller('SignupController', function ($scope, $rootScope, $http, $routeParams, $timeout) {
    $scope.alerts = '';
    $scope.form = $('#singup-form');
    $scope.first_name = '';
    $scope.last_name = '';
    $scope.email = '';
    $scope.password = '';
    $scope.password_confirmation = '';
    $scope.user_type = '';


    $scope.signup = function () {
        if (!$scope.form.validationEngine('validate')) {
            return false;
        }
        $scope.sendData();
    }

    $scope.sendData = function () {
        uiBlocker();
        var data = $.param({
            first_name: $scope.first_name,
            last_name: $scope.last_name,
            email: $scope.email,
            password: $scope.password,
            password_confirmation: $scope.password_confirmation,
        });
        $http.post($scope.__RegisterUrl, data, $scope.config)
                .success(function (response, status, headers, config) {
                    $scope.temp = angular.fromJson(response);
                    uiUnBlocker();
                    if ($scope.temp.Good) {
                        window.location = '#/lgoin';
                    } else {
                        $scope.alerts = $scope.temp.error;
                        $scope.$apply();
                        $("#error-alert").removeClass('hidden');
                    }
                })
                .error(function (response, status, header, config) {
                    uiBlocker();
                });
    };
});