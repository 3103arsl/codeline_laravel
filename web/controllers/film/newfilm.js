codeline.controller('NewFilmsController', function ($scope, $rootScope, $routeParams, $http, $timeout, $route) {


    $scope.submit = function (params) {
        if (!$('#film-form').validationEngine('validate')) {
            return false;
        }
        $scope.sendData(params);
    }

    $scope.sendData = function (params) {
        uiBlocker();
        var data = $.param(params);
        $http.post($scope.__FilmCreateUrl, data, $scope.config)
                .success(function (response, status, headers, config) {
                    uiUnBlocker();
                    if (response.Good) {
                        $scope.alerts = response.message;
                        $scope.params = angular.copy($scope.master);
                        $scope.userForm.$setPristine();

                        $scope.$apply();

                        $("#error-alert").removeClass('hidden');
                        setTimeout(function () {
                            $('#myModal').modal('hide');
                            $('#bid-form-container').remove();
                        }, 5000);
                    } else {
                        $scope.alerts = response.errors;
                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                        $("#error-alert").removeClass('hidden');
                    }
                })
                .error(function (response, status, header, config) {
                    uiBlocker();
                });
    }


});