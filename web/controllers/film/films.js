codeline.controller('FilmsController', function ($scope, $rootScope, $routeParams, $http, $timeout) {
    $scope.films = '';
    $scope.alerts = '';
    $scope.meta = '';
    $scope.pages = [];

    $scope.films = function () {
        var qString = '';
        if ($routeParams.page > 1) {
            qString = '?page=' + $routeParams.page;
        }
        $http.get($scope.__FilmsUrl + qString, $scope.config)
                .success(function (response, status, headers, config) {
                    if (response.Good) {
                        uiUnBlocker();
                        $scope.films = response.data;
                        $scope.meta = response.meta;
                        $scope.pages = $scope.generatePages($scope.meta);
                        $scope.$apply();
                    } else {
                        $scope.alerts = response.errors;
                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                        $("#error-alert").removeClass('hidden');
                    }
                })
                .error(function (response, status, header, config) {
                    uiBlocker();
                });
    }

    $scope.films();

});