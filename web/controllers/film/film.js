codeline.controller('FilmController', function ($scope, $rootScope, $routeParams, $http, $timeout, $route) {

    $scope.detail = '';


    $scope.film = function () {
        $http.get($scope.__FilmUrl + $routeParams.slug, $scope.config)
                .success(function (response, status, headers, config) {
                    uiUnBlocker();
                    $scope.detail = response.data;
                    $scope.$apply();

                    console.log($scope.detail);
                })
                .error(function (response, status, header, config) {
                    uiBlocker();
                });
    }

    $scope.film();
});