<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Http\Helpers\StringHelper;
use App\Http\Helpers\ValidatorFacade;

class RegisterController extends Controller {

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $validator = $this->validator($request);

        if ($validator->fails()) {
            return response()->json([
                        'Good' => false,
                        'error' => $validator->errors()
                            ], 200);
        }

        $user = User::where(['email' => $request->get('email')])->count();
        if ($user != 0) {
            return response()->json([
                        'Good' => false,
                        'error' => 'User already exists.'
                            ], 200);
        }

        $role = 'MEMBER';

        $user = new User();
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('password'));
        $user->user_role = $role;
        $user->is_active = 1;
        $user->save();

        return response()->json([
                    'Good' => true,
                    'message' => 'You are register successfully.'
                        ], 200);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator($data) {

        return Validator::make($data->all(), [
                    'first_name' => 'required|string|max:100',
                    'last_name' => 'required|string|max:100',
                    'email' => 'required|string|email|max:100|unique:users',
                    'password' => 'required|string|min:6|confirmed',
        ]);
    }

}
