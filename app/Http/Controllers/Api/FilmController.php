<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Helpers\StringHelper;
use App\Http\Resources\FilmResource;
use App\Http\Resources\FilmCollection;
use App\Models\User;
use App\Models\Film;
use App\Models\FilmGenre;
use App\Models\Comment;

class FilmController extends Controller {

    public function index(Request $request) {
        $currentUser = $this->currentUser;
        $films = Film::with(['genres', 'comments'])->orderBy('id', 'DESC')->paginate(1);
        return new FilmCollection($films);
    }

    public function store(Request $request) {

        $validator = $this->validateFilm($request);

        if ($validator->fails()) {
            return response()->json(['Good' => false, 'errors' => $validator->errors()]);
        }

        if (!$this->save($request)) {
            return response()->json(['Good' => false, 'error' => 'New film has not been save.']);
        }

        return response()->json(['Good' => true, 'message' => 'New film save successfully.']);
    }

    public function view(Request $request, $slug) {
        $currentUser = $this->currentUser;
        $film = Film::with(['genres', 'comments'])->where(['slug' => $slug])->first();

        return new FilmResource($film);
    }

    protected function save(Request $request) {
        $slug = StringHelper::Slug($request->get('name'), new Film);
        $fileName = '';
        if ($request->hasFile('photo')) {
            if ($request->file('photo')->isValid()) {
                $file = $request->file('photo');
                $destinationPath = 'public/uploads/images/';
                $fileNewName = str_random(50) . time() . str_random(50);
                $fileName = $fileNewName . '.' . $file->getClientOriginalExtension();
                $file->move($destinationPath, $fileName);
            }
        }

        $film = new Film();
        $film->name = $request->get('name');
        $film->slug = $slug;
        $film->description = $request->get('description');
        $film->release_date = date('Y-m-d', strtotime($request->get('release_date')));
        $film->rating = $request->get('rating');
        $film->price = $request->get('price');
        $film->country = $request->get('country');
        $film->photo = $fileName;
        $film->save();
        if (!$film) {
            return false;
        }
        $this->saveGenre($request, $film);
        //$this->sendNotification($shipperRequest);
        return true;
    }

    public function storeComment(Request $request, Film $film) {
        $currentUser = $this->currentUser;
        $validator = $this->validateComment($request);

        if ($validator->fails()) {
            return response()->json(['Good' => false, 'errors' => $validator->errors()]);
        }

        $coment = new Comment();
        $coment->user_id = ($currentUser) ? $currentUser->user_id : null;
        $coment->film_id = $film->id;
        $coment->name = $request->get('name');
        $coment->comment = $request->get('comment');
        $coment->save();

        if (!$coment) {
            return response()->json(['Good' => false, 'error' => 'New comment has not been post.']);
        }

        return response()->json(['Good' => true, 'message' => 'New comment has been post.']);
    }

    protected function saveGenre(Request $request, Film $film) {
        $genre = $request->get('genre');
        if (is_array($genre) && !empty($genre)) {
            foreach ($genre as $key => $value) {
                $filmGenre = new FilmGenre();
                $filmGenre->film_id = $film->id;
                $filmGenre->genre_id = $value;
                $filmGenre->save();
            }
        }
    }

    protected function validateFilm(Request $request) {
        return Validator::make($request->all(), [
                    "name" => 'required|string|max:100',
                    "description" => 'required|string',
                    "release_date" => 'required|string',
                    "rating" => 'required|numeric|between:1,5',
                    "price" => 'required|numeric',
                    "country" => 'required|string',
                        //"photo" => 'required|mimes:jpg,png,jpeg',
        ]);
    }

    protected function validateComment(Request $request) {
        return Validator::make($request->all(), [
                    "name" => 'required|string|max:100',
                    "comment" => 'required|string',
        ]);
    }

}
