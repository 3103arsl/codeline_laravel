<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\User;
use App\Models\UserDevice;
use App\Http\Helpers\StringHelper;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller {

    use AuthenticatesUsers;

    public function login(Request $request) {

        $validator = $this->validateLogin($request);

        if ($validator->fails()) {
            return response()->json([
                        'Good' => false,
                        'errors' => $validator->errors()
            ]);
        }

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return response()->json([
                        'Good' => false,
                        'errors' => 'Your account has been locked.'
            ]);
        }

        if (!$this->attemptLogin($request)) {
            $this->incrementLoginAttempts($request);
            return response()->json([
                        'Good' => false,
                        'errors' => 'Username or password invalid or inactive.'
            ]);
        }

        $token = StringHelper::getUniqeStr();
        $userDevice = new UserDevice();
        $userDevice->user_id = Auth::id();
        $userDevice->device_name = $request->get('device_name');
        $userDevice->device_type = $request->get('device_type');
        $userDevice->push_token = $request->get('push_token');
        $userDevice->token = $token;
        $userDevice->save();

        return response()->json([
                    'Good' => true,
                    'authToken' => $token,
                    'user' => [
                        'name' => Auth::user()->getFullName(),
                        'user_type' => 1,
                    ]
        ]);
    }

    protected function validateLogin(Request $request) {

        return Validator::make($request->all(), [
                    'email' => 'required|email',
                    'password' => 'required|string',
        ]);
    }

    protected function attemptLogin(Request $request) {
        return Auth::attempt([
                    'email' => $request->get('email'),
                    'password' => $request->get('password'),
                    'is_active' => 1,
        ]);
    }

    public function logout(Request $request) {
        $this->guard()->logout();
        $userDevice = UserDevice::where('token', $request->header('authToken'))->delete();
        if ($userDevice) {
            return response()->json([
                        'Good' => true,
            ]);
        }
        return response()->json([
                    'Good' => false,
        ]);
    }

}
