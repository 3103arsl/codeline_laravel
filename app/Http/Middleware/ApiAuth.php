<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use App\Models\UserDevice;

class ApiAuth {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

        $token = $request->header('authToken');
        if (isset($token) && $token != '') {
            $user = UserDevice::where(['token' => $token])->first();
            if ($user) {
                return $next($request);
            }
        }
        return response()->json([
                    'Good' => false,
                    'error' => 'Unauthorized request'
        ],403);
    }

}
