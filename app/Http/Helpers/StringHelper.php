<?php

namespace App\Http\Helpers;

use Illuminate\Support\Facades\Request;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use DateTime;

class StringHelper {

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  string
     * @return string
     */
    public static function Slug($string, $model) {
        $string = str_slug($string, "-");
        $count = $model::where('slug', 'like', '%' . $string . '%')->pluck('id')->toArray();
        $count = count($count);
        if ($count == 0) {
            return $string;
        }
        $count = $count + 1;
        return $string = $string . '-' . $count;
    }

    public static function slugify($string, $alternate = '-') {
        return $string = str_slug($string, $alternate);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $string
     * @param  Boolean  $flag
     * @return string
     */
    public static function GenerateTitle($string, $flag = FALSE) {
        $string = ucfirst($string);
        if (!$flag) {
            $string = str_plural($string);
        }
        return $string;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $object
     * @return array
     */
    public static function AddDefaultOption($object, $defaultValue = 'Select') {
        $return = array(0 => $defaultValue);
        if (!empty($object)) {
            foreach ($object as $obj => $val) {
                $return[$obj] = $val;
            }
        }
        return $return;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $object
     * @return array
     */
    public static function ObjectToArray($object) {
        $return = array();
        if (!empty($object)) {
            foreach ($object as $obj) {
                $return[] = $obj->category_id;
            }
        }
        return $return;
    }

    public static function getRenderHTML($string) {
        return html_entity_decode($string);
    }

    public static function getTargetURL($obj, $prepend = '') {
        if ($prepend != '') {
            $prepend = $prepend . '/';
        }
        return ($obj->url == '') ? URL::to('/' . $prepend . $obj->slug) : $obj->url;
    }

    public static function getRating($ratings) {
        $total = $ratings->count();
        if ($total > 0) {
            $sum = 0;
            foreach ($ratings as $rating) {
                $sum += $rating->rate;
            }
            $rating = round($sum / $ratings->count());
        } else {
            $rating = 0;
        }
        return view('front.snippets.rating-stars')->with(array('rating' => $rating, 'ratingData' => $ratings));
    }

    public static function timeElapsedString($datetime) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        //if (!$full)
        $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    public static function sendMail($data) {
        return $send = Mail::send('front.emails.' . $data['template'], ['data' => $data], function ($message) use ($data) {
                    if (!empty($data['attachments'])) {
                        foreach ($data['attachments'] as $attachment) {
                            if ($attachment != '') {
                                $message->attach($attachment);
                            }
                        }
                    }
                    $message->to($data['to'], $data['name'])->subject($data['subject']);
                });
    }

    public static function getLimitedText($string, $limit = 20) {
        return str_limit($string, $limit);
    }

    public static function validQueryString($string) {
        $string = filter_var($string, FILTER_SANITIZE_STRING);
        return $string;
    }

    public static function validQueryNumber($string) {
        $string = filter_var($string, FILTER_SANITIZE_NUMBER_INT);
        return $string;
    }

    public static function limitedText($string, $limit = 145) {
        $string = html_entity_decode($string);
        $string = strip_tags($string);
        $string = str_limit($string, $limit);
        return $string;
    }

    public static function formatedDate($date, $formate = 'M d, Y') {

        $newDate = strtotime($date);
        return $newDate = date($formate, $newDate);
    }

    public static function maxInstallmentAmount($price) {
        $price = ($price * 95) / 100;
        return $price;
    }

    public static function formatMoney($number, $fractional = false) {
        if ($fractional) {
            $number = sprintf('%.2f', $number);
        }
        while (true) {
            $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $number);
            if ($replaced != $number) {
                $number = $replaced;
            } else {
                break;
            }
        }
        return $number;
    }

    public static function getCompanyInfo($key) {
        return config('constants.' . $key);
    }

    public static function getCurrentURI($segment = 1) {
        return Request::segment($segment);
//        $uri = explode('/', $_SERVER['REQUEST_URI']);
//        return end($uri);
    }

    public static function getAdminURI() {
        return parent::getAdmimURI();
    }

    public static function getMediaPath($type = 'thumb') {
        if ($type != 'thumb') {
            return parent::getUploadPath();
        }

        return parent::getThumbPath();
    }

    public static function arrayToStr($arr, $speator = ' ') {
        return collect($arr)->implode($speator);
    }

    public static function replaceStr($str, $search = 'uploads/', $replace = 'public/uploads/') {
        return str_replace($search, $replace, $str);
    }

    public static function getUniqeStr() {
        return md5(time() . md5('G@@DMATCS-!FDSYG@!')) . md5(md5(time()) . time());
    }

    public static function getStatus($status) {
        if ($status == 1) {
            return Status::ACTIVE_LABEL;
        } elseif ($status == 0) {
            return Status::INACTIVE_LABEL;
        } elseif ($status == 3) {
            return Status::CANCELED_LABEL;
        }
    }

}
