<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Helpers\StringHelper;

class FilmCollection extends ResourceCollection {

    public $resource;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function __construct($resource) {
        $this->resource = $resource;
        parent::__construct($this->resource);
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request) {
        return [
            'Good' => true,
            'data' => $this->data($request)
        ];
    }

    private function data($request) {
        $data = [];
        if ($this->resource && $this->resource->count() > 0) {
            foreach ($this->resource as $resource) {
                $data[] = [
                    "id" => $resource->id,
                    "name" => $resource->name,
                    "slug" => $resource->slug,
                    "release_date" => $resource->release_date,
                    "rating" => $resource->rating,
                    "price" => $resource->price,
                    "country" => $resource->country,
                    "photo" => $resource->getPhoto(),
                    'ago' => StringHelper::timeElapsedString($resource->created_at),
                    "genres" => $resource->getGenres(),
                    "comments" => $resource->getComments()
                ];
            }
        }
        return $data;
    }

    

}
