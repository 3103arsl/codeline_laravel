<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Helpers\StringHelper;

class FilmResource extends Resource {

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function __construct($resource) {
        parent::__construct($resource);
    }

    public function toArray($request) {
        return $this->data($request);
    }

    private function data($request) {

        if ($this->resource && $this->resource->count() > 0) {
            return [
                "id" => $this->id,
                "name" => $this->name,
                "slug" => $this->slug,
                "description" => $this->description,
                "release_date" => $this->release_date,
                "rating" => $this->rating,
                "price" => $this->price,
                "country" => $this->country,
                "photo" => $this->getPhoto(),
                'ago' => StringHelper::timeElapsedString($this->created_at),
                "genres" => $this->getGenres(),
                "comments" => $this->getComments()
            ];
        }
    }

}
