<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Helpers\StringHelper;

class Film extends Model {

    protected $table = 'films';

    public function creator() {
        return $this->belongTo('App\Models\User');
    }

    public function genres() {
        return $this->belongsToMany('App\Models\Genre');
    }

    public function comments() {
        return $this->hasMany('App\Models\Comment');
    }

    public function getPhoto() {
        if ($this->photo) {
            return asset('public/uploads/images/' . $this->photo);
        }

        return null;
    }

    public function getComments() {
        $data = array();
        foreach ($this->comments as $comment) {
            $data[] = [
                'id' => $comment->id,
                'name' => $comment->name,
                'comment' => $comment->comment,
                'commenter' => ($comment->commenter) ? $comment->commenter->getFullName() : '-',
                'ago' => StringHelper::timeElapsedString($comment->created_at)
            ];
        }
        return $data;
    }

    public function getGenres() {
        $data = array();
        if ($this->genres->count() > 0) {
            foreach ($this->genres as $genre) {
                $data[] = ['id' => $genre->id, 'name' => $genre->name];
            }
        }
        return $data;
    }

}
