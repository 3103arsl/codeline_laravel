<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model {

    protected $table = 'genres';

    public function film() {
        return $this->belongTo('App\Models\Film');
    }

    public function genre() {
        return $this->hasMany('App\Models\FilmGenre');
    }

}
