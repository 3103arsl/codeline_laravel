<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FilmGenre extends Model {

    protected $table = 'film_genre';

    public function filmes() {
        return $this->belongsTo('App\Models\Film', 'film_id');
    }

    public function genres() {
        return $this->belongsTo('App\Models\Genre', 'genre_id');
    }

}
