<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDevice extends Model {

    protected $table = 'user_devices';
    protected $fillable = [
        'user_id',
        'device_name',
        'device_type',
        'push_token',
        'token',
    ];

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

}
