<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

    protected $table = 'comments';

    public function film() {
        return $this->belongsTo('App\Models\Film');
    }
    
    public function commenter() {
        return $this->belongsTo('App\Models\User');
    }

}
