<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

    use Notifiable;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function films() {
        return $this->hasMany('App\Models\Film');
    }

    public function devices() {
        return $this->hasMany('App\Models\UserDevice');
    }
    public function comments() {
        return $this->hasMany('App\Models\Comment');
    }

    public function getFullName() {
        if ($this->last_name != '') {
            return $this->first_name . ' ' . $this->last_name;
        }

        return $this->first_name;
    }

}
