<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::group(['prefix' => 'v1/'], function () {
    Route::apiResource('user', 'Api\RegisterController');
    Route::post('login', 'Api\LoginController@login');
    Route::post('logout', 'Api\LoginController@logout');


    Route::group(['prefix' => 'films'], function () {
        Route::get('/', 'Api\FilmController@index');
        Route::get('/{slug}', 'Api\FilmController@view');
        Route::post('/save', 'Api\FilmController@store');

        Route::group(['middleware' => 'api_auth'], function () {
            Route::post('/comment/{film}', 'Api\FilmController@storeComment');
        });
    });
});
