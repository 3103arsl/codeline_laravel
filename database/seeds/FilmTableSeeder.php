<?php

use Illuminate\Database\Seeder;

class FilmTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        DB::table('users')->insert([
            'first_name' => 'Arslan',
            'last_name' => 'Arif',
            'email' => '3103arsl@gmail.com',
            'is_active' => 1,
            'password' => bcrypt('member321'),
        ]);



        for ($i = 1; $i <= 3; $i++) {
            $name = 'Film ' . $i;
            DB::table('films')->insert([
                'name' => $name,
                'slug' => str_slug($name, '-'),
                'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ',
                'release_date' => '2017-11-22',
                'rating' => $i + 2,
                'price' => 500,
                'country' => 'USA'
            ]);
        }
    }

}
