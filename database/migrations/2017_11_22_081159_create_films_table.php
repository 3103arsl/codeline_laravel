<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilmsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('films', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')
                    ->references('id')->on('users');
            $table->string('name', 100)->nullable();
            $table->string('slug', 100)->nullable();
            $table->text('description')->nullable();
            $table->date('release_date')->nullable();
            $table->integer('rating')->nullable();
            $table->integer('price')->nullable();
            $table->string('country', 100)->nullable();
            $table->string('photo', 255)->nullable();
            $table->timestamps();
        });

        Schema::create('genres', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100)->nullable();
            $table->timestamps();
        });

        Schema::create('film_genre', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('film_id')->unsigned()->nullable();
            $table->foreign('film_id')
                    ->references('id')->on('films');

            $table->integer('genre_id')->unsigned()->nullable();
            $table->foreign('genre_id')
                    ->references('id')->on('genres');
            $table->timestamps();
        });

        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('film_id')->unsigned()->nullable();
            $table->string('name', 100)->nullable();
            $table->text('comment')->nullable();
            $table->timestamps();
            $table->foreign('user_id')
                    ->references('id')->on('users');
            $table->foreign('film_id')
                    ->references('id')->on('films');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('films');
    }

}
